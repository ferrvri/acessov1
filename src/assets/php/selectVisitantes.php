<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if($con && isset($req->id) && is_numeric($req->id) && isset($req->unique) && $req->unique == true){
    $stmt = $con->prepare("SELECT * from visitante where visitante.bit_deletado = '0' and vis_id = ? order by vis_nome asc");
    $stmt->bind_param('i', $req->id);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if($con && isset($req->id) && !is_numeric($req->id) && isset($req->unique) && $req->unique == true){
    $stmt = $con->prepare("SELECT * from visitante where visitante.bit_deletado = '0' and vis_rg = ? order by vis_nome asc");
    $stmt->bind_param('s', $req->id);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && !isset($req->id) && !isset($req->unique)){
    // $stmt = $con->prepare("SELECT * from visitante inner join aux_visita_morador on fk_aux_visita = vis_id 
    // inner join aux_veiculo_visitante on aux_vis_visitante_id = vis_id 
    // inner join morador on fk_aux_morador = mor_id
    // left join casa on fk_id_mor_casa = cas_id
    // inner join veiculo on aux_vis_veiculo_id = vel_id 
    // inner join veiculo_marca on fk_id_marca_veiculo = vm_id inner join veiculo_modelo on fk_id_modelo = vmo_id 
    // inner join cor on fk_id_cor = cor_id where visitante.bit_deletado = '0' and cor.bit_deletado = '0'
    // and veiculo_modelo.bit_deletado = '0' and veiculo_marca.bit_deletado = '0'");
    $stmt = $con->prepare("SELECT * from visitante where visitante.bit_deletado = '0' order by vis_nome asc");
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->id) && !isset($req->unique)){
    // $stmt = $con->prepare("SELECT *, date_format(aux_visita_morador.aux_vis_mor_data, '%d/%m/%Y as %H:%i') as aux_vis_mor_data2 from visitante
    // inner join aux_visita_morador on fk_aux_visita = vis_id 
    // inner join aux_veiculo_visitante on aux_vis_visitante_id = vis_id 
    // inner join morador on fk_aux_morador = mor_id
    // inner join casa on fk_id_mor_casa = cas_id
    // inner join veiculo on aux_vis_veiculo_id = vel_id 
    // inner join veiculo_marca on fk_id_marca_veiculo = vm_id 
    // inner join veiculo_modelo on fk_id_modelo = vmo_id 
    // inner join cor on fk_id_cor = cor_id where fk_aux_visita = ? and visitante.vis_id = ? and aux_vis_visitante_id = ? and visitante.bit_deletado = '0' and cor.bit_deletado = '0'
    // and veiculo_modelo.bit_deletado = '0' and veiculo_marca.bit_deletado = '0'");
    $stmt = $con->prepare('select *, date_format(aux_visita_morador.aux_vis_mor_data, "%d/%m/%Y as %H:%i") as aux_vis_mor_data2 from aux_visita_morador 
    inner join morador on fk_aux_morador = mor_id 
    left join casa on fk_id_mor_casa = cas_id inner 
    join visitante on fk_aux_visita = vis_id 
    inner join veiculo on fk_vel_id = vel_id 
    left join cor on fk_id_cor = cor_id 
    left join veiculo_modelo on fk_id_modelo = vmo_id 
    left join veiculo_marca on fk_id_marca = vm_id where vis_id = ? and visitante.bit_deletado = "0"');
    $stmt->bind_param('i', $req->id);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>