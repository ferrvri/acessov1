<?php
require_once('./connection.php');
$req = json_decode(file_get_contents('php://input'));

if ($con && isset($req->placa) && ($req->placa != 'NULL' || $req->marca != 'null' || $req->marca !== null)){
    $stmt = $con->prepare("insert into veiculo (vel_placa, fk_id_modelo, fk_id_cor, fk_id_marca_veiculo) values (?, ?, ?, ?)");
    $stmt->bind_param('siii', $req->placa, $req->modelo, $req->cor, $req->marca);
    
    if ($stmt->execute( )){
        // $stmt = $con->prepare("insert into aux_veiculo_visitante (aux_vis_visitante_id, aux_vis_veiculo_id) values (?, (select vel_id from veiculo order by vel_id desc limit 1))");
        // $stmt->bind_param('i', $req->fk_visita);
        // if ($stmt->execute( )){
            $stmt = $con->prepare("insert into aux_visita_morador (fk_aux_visita, fk_aux_morador, fk_vel_id) values (?, ?, (select vel_id from veiculo order by vel_id desc limit 1))");
            $stmt->bind_param('ii', $req->fk_visita, $req->fk_morador);
            if ($stmt->execute( )){    
                echo json_encode(array('status' => '0x104'));
            }else{
                echo json_encode(array('status' => '0x101'));
            }
        // }else{
        //     echo json_encode(array('status' => '0x101'));
        // }
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}

?>