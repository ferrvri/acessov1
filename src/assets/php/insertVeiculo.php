<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();

function getMorador($req, $con){
    $id = '';
    if ($con){
        $stmt = $con->prepare("SELECT mor_id from morador where mor_rg = ?");
        $stmt->bind_param('s', $req->rg);
        $stmt->execute( );
        $result = $stmt->get_result( );

        while ( $row = $result->fetch_assoc( ) ) {
            $id = $row['mor_id'];            
        }

        if (sizeof($id) > 0){
            return $id;
        }else{
            return false;
        }
    }
}

function getVisitante($req, $con){
    $id = '';
    if ($con){
        $stmt = $con->prepare("SELECT vis_id from visitante where vis_rg = ?");
        $stmt->bind_param('s', $req->rg);
        $stmt->execute( );
        $result = $stmt->get_result( );

        while ( $row = $result->fetch_assoc( ) ) {
            $id = $row['vis_id'];            
        }

        if (sizeof($id) > 0){
            return $id;
        }else{
            return false;
        }
    }
}


if ($con && isset($req->placa) && ($req->placa != 'NULL' || $req->marca != 'null' || $req->marca !== null) && isset($req->morador) && $req->morador == true && !isset($req->visitante)){
    $id = getMorador($req, $con);
    $stmt = $con->prepare("insert into veiculo (vel_placa, fk_id_modelo, fk_id_cor, fk_id_marca_veiculo) values (?, ?, ?, ?)");
    $stmt->bind_param('siii', $req->placa, $req->modelo, $req->cor, $req->marca);
    
    if ($stmt->execute( )){
        $stmt = $con->prepare("insert into aux_veiculo_morador (aux_mor_morador_id, aux_mor_veiculo_id) values (?, (select vel_id from veiculo order by vel_id desc limit 1))");
        $stmt->bind_param('i', $id);
        if ($stmt->execute( )){    
            echo json_encode(array('status' => '0x104'));
        }else{
            echo json_encode(array('status' => '0x101'));
        }
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->placa) && ($req->placa != 'NULL' || $req->marca != 'null' || $req->marca !== null) && !isset($req->morador) && isset($req->visitante) && $req->visitante == true ){
    $id = getVisitante($req, $con);
    $stmt = $con->prepare("insert into veiculo (vel_placa, fk_id_modelo, fk_id_cor, fk_id_marca_veiculo) values (?, ?, ?, ?)");
    $stmt->bind_param('siii', $req->placa, $req->modelo, $req->cor, $req->marca);
    
    if ($stmt->execute( )){
        $stmt = $con->prepare("INSERT INTO aux_visita_morador (fk_aux_visita, fk_aux_morador, fk_vel_id) values ((select vis_id from visitante order by vis_id desc limit 1), ?, (select vel_id from veiculo order by vel_id desc limit 1))");
        $stmt->bind_param('i', $req->moradorId);
        if ($stmt->execute( )){
            echo json_encode(array('status' => '0x104'));
        }else{
            echo json_encode(array('status' => '0x101'));
        }
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>