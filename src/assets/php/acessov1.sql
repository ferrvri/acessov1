CREATE DATABASE acessov1;

USE acessov1;

CREATE TABLE usuario (
    usu_id int primary key auto_increment,
    usu_nome varchar(25),
    usu_email varchar(25),
    usu_login varchar(25),
    usu_senha varchar(25),
    bit_deletado varchar(1) default '0'
);

CREATE TABLE visitante (
    vis_id int primary key auto_increment,
    vis_nome varchar(50),
    vis_rg varchar(20),
    vis_telefone varchar(20),
    vis_foto LONGTEXT,
	bit_deletado varchar(1) default '0'
);

CREATE TABLE casa(
    cas_id int primary key auto_increment,
    cas_rua_nome varchar(30),
    cas_quadra varchar(30),
    cas_lote varchar(30),
    bit_deletado varchar(1) default '0'
);

CREATE TABLE cor(
    cor_id int primary key auto_increment,
    cor_nome varchar(25),
	bit_deletado varchar(1) default '0'
);

CREATE TABLE veiculo_marca(
    vm_id int primary key auto_increment,
    vm_nome varchar(25),
	bit_deletado varchar(1) default '0'
);

CREATE TABLE veiculo_modelo(
    vmo_id int primary key auto_increment,
    vmo_nome varchar(25),
    fk_id_marca int,
	bit_deletado varchar(1) default '0',
    foreign key (fk_id_marca) references veiculo_marca (vm_id)
);

CREATE TABLE veiculo(
    vel_id int auto_increment primary key,
    vel_placa varchar(25),
    fk_id_modelo int,
    fk_id_cor int,
    fk_id_marca_veiculo int,
	bit_deletado varchar(1) default '0',
    foreign key (fk_id_modelo) references veiculo_modelo(vmo_id),
    foreign key (fk_id_cor) references cor(cor_id),
    foreign key (fk_id_marca_veiculo) references veiculo_marca(vm_id)
);

CREATE TABLE morador(
    mor_id int primary key auto_increment,
    mor_nome varchar(50),
    mor_telefone varchar(25),
    mor_rg varchar(25),
    mor_foto longtext,
    fk_id_mor_casa int,
    bit_deletado varchar(1) default '0',
    foreign key (fk_id_mor_casa) references casa(cas_id)
);

CREATE TABLE aux_veiculo_morador(
    aux_mor_id int primary key auto_increment,
    aux_mor_morador_id int,
    aux_mor_veiculo_id int,
    foreign key (aux_mor_morador_id) references morador(mor_id),
    foreign key (aux_mor_veiculo_id) references veiculo(vel_id)
);

-- CREATE TABLE aux_veiculo_visitante(
--     aux_vis_id int primary key auto_increment,
--     aux_vis_visitante_id int,
--     aux_vis_veiculo_id int,
--     foreign key (aux_vis_visitante_id) references visitante(vis_id),
--     foreign key (aux_vis_veiculo_id) references veiculo(vel_id)
-- );

CREATE TABLE aux_visita_morador(
    aux_vis_mor_id int primary key auto_increment,
    fk_aux_visita int,
    fk_aux_morador int,
    aux_vis_mor_data datetime default now(),
    fk_vel_id int,
    foreign key (fk_aux_visita) references visitante(vis_id),
    foreign key (fk_aux_morador) references veiculo(vel_id),
    foreign key (fk_aux_morador) references morador(mor_id)
);

insert into usuario (usu_nome, usu_email, usu_login, usu_senha) values ('Administrador', 'adm@admin.com', 'admin', '123456');
insert into veiculo_marca (vm_nome) values ('Chevrolet'), ('Audi'), ('Ford'), ('Fiat');
insert into cor (cor_nome) values ('Prata'), ('Preto'), ('Vinho'), ('Grafite'), ('Branco'), ('Vermelho');