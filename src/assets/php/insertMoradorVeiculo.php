<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->placa) && sizeof($req->placa) > 0 && $req->placa !== null){
    $stmt = $con->prepare("INSERT INTO veiculo (vel_placa, fk_id_modelo, fk_id_cor, fk_id_marca_veiculo) values (?, ?, ?, ?)");
    $stmt->bind_param('siii', $req->placa, $req->modelo, $req->cor, $req->marca);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        $stmt = $con->prepare("INSERT INTO aux_veiculo_morador (aux_mor_morador_id, aux_mor_veiculo_id) values (?, (select vel_id from veiculo order by vel_id desc limit 1))");
        $stmt->bind_param('i', $req->id);
        if ($stmt->execute( )){
            echo json_encode(array('status' => '0x104'));
        }else{
            echo json_encode(array('status' => '0x101'));    
        }
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}

?>