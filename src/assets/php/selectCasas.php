<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && !isset($req->unique) && !isset($req->id)){
    $stmt = $con->prepare("SELECT * from casa where bit_deletado = '0'");
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->unique) && $req->unique == true && isset($req->id)){
    $stmt = $con->prepare("SELECT * from casa where cas_id = ? and bit_deletado = '0'");
    $stmt->bind_param('i', $req->id);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }

}


?> 