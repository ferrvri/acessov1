<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null){
    $foto = $req->foto . '.png';
    $stmt = $con->prepare("INSERT INTO morador (mor_nome, mor_telefone, mor_rg, mor_foto, fk_id_mor_casa) values (?, ?, ?, ?, ?)");
    $stmt->bind_param('ssssi', $req->nome, $req->telefone, $req->rg, $foto, $req->fk_id_mor_casa);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}

?>