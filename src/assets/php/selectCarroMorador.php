<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->id)){
    $stmt = $con->prepare("select * from aux_veiculo_morador inner join morador on aux_mor_morador_id = mor_id inner join veiculo on aux_mor_veiculo_id = vel_id left join cor on fk_id_cor = cor_id left join veiculo_marca on fk_id_marca_veiculo = vm_id left join veiculo_modelo on fk_id_modelo = vmo_id where mor_id = ?");
    $stmt->bind_param('i', $req->id);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>