<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null){
    $stmt = $con->prepare("INSERT INTO veiculo_marca (vm_nome) values (?)");
    $stmt->bind_param('s', $req->nome);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}

?>