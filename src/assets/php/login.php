<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con){
    $stmt = $con->prepare("SELECT usu_email, usu_nome, usu_id FROM usuario where usu_login = ? and usu_senha = ?");
    $stmt->bind_param('ss', $req->user, $req->pass);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>