<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null && isset($req->type) && $req->type == 'updatewfoto'){
    $stmt = $con->prepare("UPDATE visitante SET vis_nome = ?, vis_rg = ?, vis_telefone = ? where vis_id = ?");
    $stmt->bind_param('sssi', $req->nome, $req->rg, $req->telefone, $req->id);
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null && isset($req->type) && $req->type == 'update'){
    $foto = $req->foto . '.png';
    $stmt = $con->prepare("UPDATE visitante SET vis_nome = ?, vis_rg = ?, vis_telefone = ?, vis_foto = ? where vis_id = ?");
    $stmt->bind_param('ssssi', $req->nome, $req->rg, $req->telefone, $foto, $req->id);
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null && !isset($req->type)){
    $foto = $req->foto . '.png';
    $stmt = $con->prepare("INSERT INTO visitante (vis_nome, vis_rg, vis_telefone, vis_foto) values (?, ?, ?, ?)");
    $stmt->bind_param('ssss', $req->nome, $req->rg, $req->telefone, $foto);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        // $stmt = $con->prepare("INSERT INTO aux_visita_morador (fk_aux_visita, fk_aux_morador, fk_vel_id) values ((select vis_id from visitante order by vis_id desc limit 1), ?, (select vel_id from veiculo order by vel_id desc limit 1))");
        // $stmt->bind_param('i', $req->moradorId);
        // if ($stmt->execute( )){
            echo json_encode(array('status' => '0x104'));
        // }else{
        //     echo json_encode(array('status' => '0x101'));    
        // }
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}

?>