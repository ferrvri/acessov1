<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con){
    $stmt = $con->prepare("SELECT * from veiculo_modelo order by vmo_nome asc");
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>