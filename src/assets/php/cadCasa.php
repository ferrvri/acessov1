<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null && !isset($req->type)){
    $stmt = $con->prepare("INSERT INTO casa (cas_rua_nome, cas_quadra, cas_lote) values (?, ?, ?)");
    $stmt->bind_param('sss', $req->nome, $req->quadra, $req->lote);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->nome) && sizeof($req->nome) > 0 && $req->nome !== null && isset($req->type) && $req->type == 'update'){
    $stmt = $con->prepare("UPDATE casa set cas_rua_nome = ?, cas_quadra = ?, cas_lote = ? where cas_id = ?");
    $stmt->bind_param('sssi', $req->nome, $req->quadra, $req->lote, $req->id);
    // echo $stmt->execute();
    if ($stmt->execute( )){
        echo json_encode(array('status' => '0x104'));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}


?>