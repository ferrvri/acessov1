<?php 

require_once('./connection.php');

$req = json_decode(file_get_contents('php://input'));
$r = array();
if ($con && isset($req->search) && $req->search == false){
    $stmt = $con->prepare("SELECT mor_id, mor_nome, mor_rg, mor_telefone, mor_foto, cas_rua_nome, cas_quadra, cas_lote from morador inner join casa on fk_id_mor_casa = cas_id where morador.bit_deletado = '0' and casa.bit_deletado = '0' order by mor_nome asc");
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}else if ($con && isset($req->search) && $req->search == true){
    $stmt = $con->prepare("SELECT mor_id, mor_nome, mor_rg, mor_telefone, mor_foto, cas_rua_nome, cas_quadra, cas_lote from morador inner join casa on fk_id_mor_casa = cas_id where morador.mor_rg LIKE CONCAT('%', ?, '%') and morador.bit_deletado = '0' and casa.bit_deletado = '0'  order by mor_nome asc");
    $stmt->bind_param('s', $req->nome);
    $stmt->execute( );
    $result = $stmt->get_result( );

    while ( $row = $result->fetch_assoc( ) ) {
        $r[] = $row;
    }
    
    if (sizeof($r) > 0){
        echo json_encode(array('status' => '0x104', 'result' => $r));
    }else{
        echo json_encode(array('status' => '0x101'));
    }
}



?>