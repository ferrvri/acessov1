<?php
    require_once('./connection.php');
    
    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

    $req = json_decode(file_get_contents('php://input'));
    $title = tirarAcentos($req->title);
    if (isset($req->title)){
        if (file_exists('./uploads/'.$title)){
            echo json_encode(array('status' => '0x102'));
        }else{
            $f = './uploads/'. $title . '.png';
            $data = base64_decode($req->anexo);
        
            if(file_put_contents($f, $data)){
                echo json_encode(array('status' => '0x104', 'foto' => $title));
            }  else {
                echo json_encode(array('status' => '0x101'));
            }
        }
    }
?>
