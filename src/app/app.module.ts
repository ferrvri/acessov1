import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { LoadingFullComponent } from './loading-full/loading-full.component';
import { CadVisitanteComponent } from './cad-visitante/cad-visitante.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InnerloadComponent } from './innerload/innerload.component';
import { CadMoradorComponent } from './cad-morador/cad-morador.component';
import { CadCasaComponent } from './cad-casa/cad-casa.component';
import { CadCartaoComponent } from './cad-cartao/cad-cartao.component';
import { ListaVisitanteComponent } from './lista-visitante/lista-visitante.component';
import { ListaMoradorComponent } from './lista-morador/lista-morador.component';
import { HistoricoVisitaComponent } from './historico-visita/historico-visita.component';
import { NovaVisitaComponent } from './nova-visita/nova-visita.component';
import { ListaCarroComponent } from './lista-carro/lista-carro.component';
import { AddCarroComponent } from './add-carro/add-carro.component';
import { ListaCasaComponent } from './lista-casa/lista-casa.component';
import { RegistrosEntradaComponent } from './registros-entrada/registros-entrada.component';
import { RegistroMoradorComponent } from './registro-morador/registro-morador.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'loadingfull', component: LoadingFullComponent},
  {path: 'main', component: MainComponent, children: [
    {path: 'innerload', component: InnerloadComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'cadVisitante', component: CadVisitanteComponent},
    {path: 'listaVisitante', component: ListaVisitanteComponent},
    {path: 'historicoVisita', component: HistoricoVisitaComponent},
    {path: 'novaVisita', component: NovaVisitaComponent},
    {path: 'cadMorador', component: CadMoradorComponent},
    {path: 'listaMorador', component: ListaMoradorComponent},
    {path: 'listaCarros', component: ListaCarroComponent},
    {path: 'addCarro', component: AddCarroComponent},
    {path: 'cadCasa', component: CadCasaComponent},
    {path: 'listaCasa', component: ListaCasaComponent},
    {path: 'cadCartao', component: CadCartaoComponent},
    {path: 'registros', component: RegistrosEntradaComponent},
    {path: 'registrosMorador', component: RegistroMoradorComponent}
  ]}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    LoadingFullComponent,
    CadVisitanteComponent,
    DashboardComponent,
    InnerloadComponent,
    CadMoradorComponent,
    CadCasaComponent,
    CadCartaoComponent,
    ListaVisitanteComponent,
    ListaMoradorComponent,
    HistoricoVisitaComponent,
    NovaVisitaComponent,
    ListaCarroComponent,
    AddCarroComponent,
    ListaCasaComponent,
    RegistrosEntradaComponent,
    RegistroMoradorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {useHash: true}),
    HttpModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
