import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-morador',
  templateUrl: './lista-morador.component.html',
  styleUrls: ['./lista-morador.component.css']
})
export class ListaMoradorComponent implements OnInit {

  imageDialog: boolean = false;

  moradores = [];
  pageParams: any;

  constructor( private _http: Http, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'search'){
      this._http.post(
        'http://localhost/acessov1/selectMoradores.php',
        {
          search: true,
          nome: this.pageParams.params.query
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            let o = {
              mor_id: element.mor_id,
              mor_nome: element.mor_nome,
              mor_rg: element.mor_rg,
              mor_foto: element.mor_foto,
              mor_telefone: element.mor_telefone,
              cas_rua_nome: element.cas_rua_nome,
              cas_quadra: element.cas_quadra,
              cas_lote: element.cas_lote,
              menuRow: false
            }
            this.moradores.push(o);
          });
        }
      });
    }else{
      this._http.post(
        'http://localhost/acessov1/selectMoradores.php',
        {
          search: false
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            let o = {
              mor_id: element.mor_id,
              mor_nome: element.mor_nome,
              mor_rg: element.mor_rg,
              mor_telefone: element.mor_telefone,
              mor_foto: element.mor_foto,
              cas_rua_nome: element.cas_rua_nome,
              cas_quadra: element.cas_quadra,
              cas_lote: element.cas_lote,
              menuRow: false
            }
            this.moradores.push(o);
          });
        }
      });
    }
  }

  removeMorador(m){
    let c = confirm('\r\tMNTI - Solutions\r\n\r\nDeseja realmente remover esse morador?');
    if (c == true){
      this._http.post(
        'http://localhost/acessov1/removeMorador.php',
        {
          id: m.mor_id
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          window.alert('\r\tMNTI - Solutions\r\n\r\nRemovido com sucesso!');
          this._router.navigate(['/main/innerload', {page: '/main/listaMorador'}]);
        }else if (response.json().status == '0x101'){
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na remoção de morador');
        }
      });
    }
  }

  beforeScroll : number = 0;
  image: string = '';

  openImageDialog(m){
    console.log(m);
    this.imageDialog = true;
    this.image = m.mor_foto;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  closeImageDialog(){
    this.imageDialog = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }
  
}
