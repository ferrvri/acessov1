import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMoradorComponent } from './lista-morador.component';

describe('ListaMoradorComponent', () => {
  let component: ListaMoradorComponent;
  let fixture: ComponentFixture<ListaMoradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMoradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMoradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
