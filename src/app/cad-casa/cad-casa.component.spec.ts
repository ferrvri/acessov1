import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadCasaComponent } from './cad-casa.component';

describe('CadCasaComponent', () => {
  let component: CadCasaComponent;
  let fixture: ComponentFixture<CadCasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadCasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadCasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
