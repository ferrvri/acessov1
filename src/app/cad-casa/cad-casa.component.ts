import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-cad-casa',
  templateUrl: './cad-casa.component.html',
  styleUrls: ['./cad-casa.component.css']
})
export class CadCasaComponent implements OnInit {

  pageParams: any;

  constructor( private _router: Router, private _activatedRoute: ActivatedRoute, private _http: Http) { }

  ngOnInit() { 
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar'){
      this._http.post(
        'http://localhost/acessov1/selectCasas.php',
        {
          id: this.pageParams.params.id,
          unique: true
        }
      ).subscribe((response) => {
        if (response.json().status == '0x104'){
          this.nome = response.json().result[0].cas_rua_nome;
          this.quadra = response.json().result[0].cas_quadra;
          this.lote = response.json().result[0].cas_lote;
        }else if (response.json().status == '0x101'){
          window.alert('Erro na seleção de casa');
        }
      });
    }
  }

  nome: string = '';
  quadra: string = '';
  lote: string = '';

  errorNome = {required: null};
  errorQuadra = {required: null};
  errorLote = {required: null};

  validation(){
    try{  
      if (this.nome.length < 1){
        this.errorNome.required = true
      }else{
        this.errorNome.required = null;
      }
  
      if (this.lote.length < 1){
        this.errorLote.required = true
      }else{
        this.errorLote.required = null;
      }

      if(this.quadra.length < 1){
        this.errorQuadra.required = true;
      }else{
        this.errorQuadra.required = null;
      }

      return true;
    }catch(e){
      return false;
    }
  }

  cadCasa(){
    if (this.pageParams.params.page == 'editar'){
      if (this.validation() == true && this.nome.length > 0 && this.lote.length > 0 && this.quadra.length > 0){
        this._http.post(
          'http://localhost/acessov1/cadCasa.php',
          {
            id: this.pageParams.params.id,
            type: 'update',
            nome: this.nome,
            lote: this.lote,
            quadra: this.quadra
          }
        ).subscribe( (response) => {
          if (response.json().status == '0x104'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nCasa alterada com sucesso!');
            this._router.navigate(['/main/innerload', {page: '/main/cadCasa'}]);
          }else if (response.json().status == '0x101'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nErro no cadastramento de casa');
          }
        });
      }else{
        window.alert('\r\tMNTI - Solutions\r\n\r\nPreencha todos os campos!');
      }
    }else{
      if (this.validation() == true && this.nome.length > 0 && this.lote.length > 0 && this.quadra.length > 0){
        this._http.post(
          'http://localhost/acessov1/cadCasa.php',
          {
            nome: this.nome,
            lote: this.lote,
            quadra: this.quadra
          }
        ).subscribe( (response) => {
          if (response.json().status == '0x104'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nCasa cadastrada com sucesso!');
            this._router.navigate(['/main/innerload', {page: '/main/cadCasa'}]);
          }else if (response.json().status == '0x101'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nErro no cadastramento de casa');
          }
        });
      }else{
        window.alert('\r\tMNTI - Solutions\r\n\r\nPreencha todos os campos!');
      }
    }
  }

}
