import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

declare var $: any;
declare var Date:any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  userData: any = [];

  constructor(private _router: Router, private _http: Http) { }

  ngOnInit() {
    if (!localStorage.getItem('session')){
      this._router.navigate(['login']);
    }
    this.userData.push(JSON.parse(localStorage.getItem('session')));
    console.log(JSON.parse(localStorage.getItem('session')).expires.substring(0,10));
    var date1 = new Date(new Date.now());
    var date2 = new Date(JSON.parse(localStorage.getItem('session')).expires.substring(0,10));
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = timeDiff / (1000 * 3600 * 24); 
    // console.log('diff days -> '+ diffDays);
    if (diffDays >= 1){
      window.alert('\tMNTI - Solutions\r\n\r\nSessão expirada!');
      localStorage.removeItem('session');
      localStorage.removeItem('local');
      this._router.navigate(['login']);
    }else{
      this._router.navigate(['/main/dashboard']);
      $('#searchInputRG').mask('00.000.000-0');
    }
  }


  logOut(){
    localStorage.removeItem('session');
    localStorage.removeItem('local');
    this._router.navigate(['login']);
  }

  searchQuery: string = '';

  search(event: KeyboardEvent){
    let s = (<HTMLInputElement>document.getElementById('searchInputRG')).value;
    if (event.keyCode == 13){
      if (this.tipoPesquisa == 'Tipo de pesquisa'){
        window.alert('\r\tMNTI - Solutions\r\n\r\nSelecione o tipo de pesquisa primeiro!');
      }else{
        if (s.length < 1){
          window.alert('\r\tMNTI - Solutions\r\n\r\nO campo deve ser preenchido!');
        }else if(this.tipoPesquisa == 'Morador'){
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaMorador', {page: 'search', query: s}]);
          }, 1500);
        }else if (this.tipoPesquisa == 'Visitante'){
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaVisitante', {page: 'search', query: s}]);
          }, 1500);
        }
      }
    }
  }

  tipoPesquisaState:boolean = false;
  tipoPesquisa: string = 'Tipo de pesquisa';


  setPesquisa(p){
    this.tipoPesquisaState = false;
    this.tipoPesquisa = p;
  }

}
