import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCasaComponent } from './lista-casa.component';

describe('ListaCasaComponent', () => {
  let component: ListaCasaComponent;
  let fixture: ComponentFixture<ListaCasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
