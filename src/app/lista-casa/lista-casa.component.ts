import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-casa',
  templateUrl: './lista-casa.component.html',
  styleUrls: ['./lista-casa.component.css']
})
export class ListaCasaComponent implements OnInit {

  tableContent: any = [];

  constructor(private _http: Http, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._http.post(
      'http://localhost/acessov1/selectCasas.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          element.menuRow = false;
          this.tableContent.push(element);
        });
      }
    });
  }

  removeCasa(t){
    let c = confirm('\tMNTI - Solutions\r\n\r\nDeseja realmente remover?');
    if (c == true){
      this._http.post(
        'http://localhost/acessov1/removeCasa.php',
        {
          id: t.cas_id
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          window.alert('\tMNTI - Solutions\r\n\r\nRemovido com sucesso!');
          this._router.navigate(['/main/innerload', {page: '/main/listaCasa'}]);
        }else if (response.json().status == '0x101'){
          window.alert('Erro na remoção de casa');
        }
      });
    }
  }

}
