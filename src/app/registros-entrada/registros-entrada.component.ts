import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registros-entrada',
  templateUrl: './registros-entrada.component.html',
  styleUrls: ['./registros-entrada.component.css']
})
export class RegistrosEntradaComponent implements OnInit {

  registros: any = [];
  searchQuery: string = '';

  constructor(private _http: Http, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._http.post(
      'http://localhost/acessov1/selectVisitaRegistro.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.registros.push(element);
        });
        this.resultFilterMorador = this.registros;
      }
    });
  }

  resultFilterMorador: any = [];

  filterMorador(model, arr){
    if (model.length < 1){
      this.resultFilterMorador = this.registros;
    }else{
      var regexp = new RegExp((".*"+model.split("")[0] +('.*')+ ".*"), "i");
      this.resultFilterMorador = arr.filter( (e) => {
        return regexp.test(e.mor_nome);
      });
    }
  }


}
