import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';

declare var $: any;

@Component({
  selector: 'app-cad-morador',
  templateUrl: './cad-morador.component.html',
  styleUrls: ['./cad-morador.component.css']
})
export class CadMoradorComponent implements OnInit {

  @ViewChild('video') video;
  @ViewChild('myCanvas') canvas;

  error:any = { name: '', message: ''};

  errorNome = {required: null};
  errorRG = {required: null};
  errorTelefone = {required: null};
  errorFoto = {required: null};
  errorCasa = {required: null};
  errorModelo = {required: null};
  errorMarca = {required: null};
  errorCor = {required: null};
  errorPlaca = {required: null};
  errorCartao = {required: null};

  ctx: any;

  state: boolean = false;
  casas: any = [];
  
  nome: string = '';
  rg: string = '';
  telefone: string = '';
  camData: any = [];
  
  casa: string = 'Selecione';

  casaIndex: number = 0;

  marcas: any = [];

  marca: string = 'Selecione';
  marcaIndex: number = 0;

  placa: string = '';

  cartao: string = '';

  pageParams: any;
  
  foto: string = '';
  editPhoto: boolean = false;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _http: Http) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    $('#telCadMoradorInput').mask("(00) 0 0000-0000");
    $('#rgCadMoradorInput').mask("00.000.000-0");
    $('#placaMoradorInput').mask("AAA-0000",{'translation': {
      A: {pattern: /[A-Za-z]/g}
    }});

    if (this.pageParams.params.page && this.pageParams.params.page == 'editar'){
      this._http.post(
        'http://localhost/acessov1/selectCasas.php',
        {}
      ).subscribe( (response) => { 
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            this.casas.push(element);
          });
          this.resultFilterCasas = this.casas;
        }
      });

      this._http.post(
        'http://localhost/acessov1/selectMoradores.php',
        {
          unique: true,
          id: this.pageParams.params.id
        }
      ).subscribe( (response) => { 
        if (response.json().status == '0x104'){
          this.nome = response.json().result[0].mor_nome;
          this.rg = response.json().result[0].mor_rg;
          this.casaIndex = response.json().result[0].cas_id;
          this.casa = 'Rua: ' + response.json().result[0].cas_rua_nome + ' | Quadra :' + response.json().result[0].cas_quadra + ' | Lote: '+ response.json().result[0].cas_lote;
          this.telefone = response.json().result[0].mor_telefone;
          this.cartao = response.json().result[0].mor_cartao;
          this.foto = response.json().result[0].mor_foto;
        }
      });
    }else{
      this._http.post(
        'http://localhost/acessov1/selectCasas.php',
        {}
      ).subscribe( (response) => { 
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            this.casas.push(element);
          });
          this.resultFilterCasas = this.casas;
        }
      });

      this._http.post(
        'http://localhost/acessov1/selectMarcas.php',
        {}
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            this.marcas.push(element);
          });
          this.resultFilterMarcas = this.marcas;
        }
      });

      this._http.post(
        'http://localhost/acessov1/selectCores.php',
        {}
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            this.cores.push(element);
          });
          this.resultFilterCores = this.cores;
        }
      });

      this._http.post(
        'http://localhost/acessov1/selectModelos.php',
        {}
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          console.log(response.json());
          response.json().result.forEach(element => {
            this.modelos.push(element);
          });
          this.resultFilterModelos = this.modelos;
        }
      });
    }
  }

  ngAfterViewInit() {
    if (!this.pageParams.params.page || this.editPhoto){
      const _canvas = this.canvas.nativeElement;
      const _video = this.video.nativeElement;

      this.ctx = _canvas.getContext('2d');
      this.ctx.translate(_canvas.width, 0);
      this.ctx.scale(-1, 1);
      
      if((<Navigator>navigator).mediaDevices.getUserMedia){
        (<Navigator>navigator).mediaDevices.getUserMedia({video: true, audio: false}).then(
          (stream) => {
            _video.srcObject = stream;
          },
          (error) => {
            switch(error.name){
              case 'NotFoundError':
                this.error.name = 'Webcam não encontrada!';
                this.error.message = 'Por favor verifique a integridade do dispositivo';
              break;
              case 'PermissionDeniedError':
                this.error.name = 'Webcam não encontrada!';
                this.error.message = 'Por favor verifique as permições';
              break;
              default:
                this.error.name = 'Erro!';
                this.error.message = 'Por favor verifique';
              break;
            }
        });
      }
    }
  }

  allowEditPhoto(){
    this.editPhoto = true;
    this.foto = '';
    setTimeout( () => {
      this.ngAfterViewInit();
    }, 200);
  }

  takeOtherPhoto() {
    const _video = this.video.nativeElement;
    const _canvas = this.canvas.nativeElement;

    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.drawImage(_video, 0, 0, _video.width, _video.height);

    document.getElementById('myCanvas').style.display = 'block';
    document.getElementById('video').style.display = 'none';
    this.camData.push(_canvas.toDataURL().replace('data:image/png;base64,', ''));
  }

  retryOtherPhoto(){
    this.camData = [];
    document.getElementById('myCanvas').style.display = 'none';
    document.getElementById('video').style.display = 'block';
  }

  takePhoto() {
    const _video = this.video.nativeElement;
    const _canvas = this.canvas.nativeElement;

    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.drawImage(_video, 0, 0, _video.width, _video.height);

    document.getElementById('myCanvas').style.display = 'block';
    document.getElementById('video').style.display = 'none';
    this.camData.push(_canvas.toDataURL().replace('data:image/png;base64,', ''));
  }

  retryPhoto(){
    this.camData = [];
    document.getElementById('myCanvas').style.display = 'none';
    document.getElementById('video').style.display = 'block';
  }


  refreshContent(){
    this._router.navigate(['/main/innerload', {page: '/main/cadMorador'}]);
  }

  validation(){
    try{

      if (this.telefone.length < 1){
        this.errorTelefone.required = true
      }else{
        this.errorTelefone.required = null;
      }
  
      if (this.nome.length < 1){
        this.errorNome.required = true
      }else{
        this.errorNome.required = null;
      }
  
      if (this.rg.length < 1){
        this.errorRG.required = true
      }else{
        this.errorRG.required = null;
      }

      if(this.casa.length < 1 || this.casa == 'Selecione'){
        this.errorCasa.required = true;
      }else{
        this.errorCasa.required = null;
      }

      if(this.marca.length < 1 || this.marca == 'Selecione'){
        this.errorMarca.required = true;
      }else{
        this.errorMarca.required = null;
      }

      if(this.modelo.length < 1 || this.modelo == 'Selecione'){
        this.errorModelo.required = true;
      }else{
        this.errorModelo.required = null;
      }

      if(this.cor.length < 1 || this.cor == 'Selecione'){
        this.errorCor.required = true;
      }else{
        this.errorCor.required = null;
      }

      if(this.placa.length < 1){
        this.errorPlaca.required = true;
      }else{
        this.errorPlaca.required = null;
      }

      if(this.cartao.length < 1){
        this.errorCartao.required = true;
      }else{
        this.errorCartao.required = null;
      }
      return true;
    }catch(e){
      return false;
    }
  }
  editValidation(){
    try{

      if (this.telefone.length < 1){
        this.errorTelefone.required = true
      }else{
        this.errorTelefone.required = null;
      }
  
      if (this.nome.length < 1){
        this.errorNome.required = true
      }else{
        this.errorNome.required = null;
      }
  
      if (this.rg.length < 1){
        this.errorRG.required = true
      }else{
        this.errorRG.required = null;
      }
      return true;
    }catch(e){
      return false;
    }
  }

  cadMorador(){
    if (this.pageParams.params.page == 'editar'){
      if (this.editValidation() == true && this.telefone.length > 0 && this.nome.length > 0 && this.rg.length > 0){
        let noffile = this.nome.trim().replace(' ', '') +'-'+ Math.ceil(Math.random() * 100);
        if (this.camData.length > 0){
          this._http.post(
            'http://localhost/acessov1/saveFoto.php',
            {
              title: noffile,
              // anexo: '2132'
              anexo: this.camData[0]
            }
          ).subscribe((response) => {
            if (response.json().status == '0x104'){
              this._http.post(
                'http://localhost/acessov1/cadMorador.php',
                {
                  id: this.pageParams.params.id,
                  nome: this.nome,
                  type: 'update',
                  telefone: this.telefone,
                  rg: (<HTMLInputElement>document.getElementById('rgCadMoradorInput')).value,
                  foto: response.json().foto,
                  cartao: this.cartao,
                  fk_id_mor_casa: this.casaIndex
                }
              ).subscribe( (response) => {
                if (response.json().status == '0x104'){
                  window.alert('\r\tMNTI - Solutions\r\n\r\Morador editado com sucesso!');
                  this._router.navigate(['/main/innerload', {page: '/main/listaMorador'}]);
                }else if (response.json().status == '0x101'){
                  window.alert('\r\tMNTI - Solutions\r\n\r\nErro na edição de morador');
                }
              });
            }
          });
        }else{
          this._http.post(
            'http://localhost/acessov1/cadMorador.php',
            {
              id: this.pageParams.params.id,
              type: 'updatewfoto',
              nome: this.nome,
              telefone: this.telefone,
              cartao: this.cartao,
              rg: (<HTMLInputElement>document.getElementById('rgCadVisitaInput')).value
            }
          ).subscribe( (response) => {
            if (response.json().status == '0x104'){
              window.alert('\tMNTI - Solutions\r\n\r\Morador alterado com sucesso!');
              this._router.navigate(['/main/innerload', {page: '/main/listaMorador'}]);
            }else{
              window.alert('Erro na alteração de morador');
            }
          });
        }
      }
    }else{
      if (this.validation() == true && this.telefone.length > 0 && this.nome.length > 0 && this.rg.length > 0  
      && this.placa.length > 0 && this.camData.length > 0 && this.cartao.length > 0){
        let noffile = this.nome.trim().replace(' ', '') +'-'+ Math.ceil(Math.random() * 100);
        this._http.post(
          'http://localhost/acessov1/saveFoto.php',
          {
            title: noffile,
            // anexo: '2132'
            anexo: this.camData[0]
          }
        ).subscribe((response) => {
          if (response.json().status == '0x104'){
            this._http.post(
              'http://localhost/acessov1/cadMorador.php',
              {
                nome: this.nome,
                telefone: this.telefone,
                rg: (<HTMLInputElement>document.getElementById('rgCadMoradorInput')).value,
                foto: response.json().foto,
                cartao: this.cartao,
                fk_id_mor_casa: this.casaIndex
              }
            ).subscribe( (response) => {
              if (response.json().status == '0x104'){
                this._http.post(
                  'http://localhost/acessov1/insertVeiculo.php',
                  {
                    morador: true,
                    rg: (<HTMLInputElement>document.getElementById('rgCadMoradorInput')).value,
                    modelo: this.modeloIndex,
                    marca: this.marcaIndex,
                    cor: this.corIndex,
                    placa: this.placa 
                  }
                ).subscribe( (response) => {
                  console.log(response);
                  if (response.json().status == '0x104'){
                    window.alert('\r\tMNTI - Solutions\r\n\r\Morador cadastrado com sucesso!');
                    this._router.navigate(['/main/innerload', {page: '/main/cadMorador'}]);
                  }else if (response.json().status == '0x101'){
                    window.alert('\r\tMNTI - Solutions\r\n\r\nErro no cadastramento de veiculo');
                  }
                });
              }else if (response.json().status == '0x101'){
                window.alert('\r\tMNTI - Solutions\r\n\r\nErro no cadastramento de morador');
              }
            });
          }else if (response.json().status == '0x101'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção na foto!');
          }else if (response.json().status == '0x102'){
            window.alert('\r\tMNTI - Solutions\r\n\r\nA foto ja existe no sistema');
          }
        })
      }else if (this.camData.length < 1){
        window.alert('\r\tMNTI - Solutions\r\n\r\nÉ necessario tirar uma foto!');
      }else{
        window.alert('\r\tMNTI - Solutions\r\n\r\nPreencha todos os campos!');
      }
    }
  }

  resultFilterCasas : any = [];

  filterCasas(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterCasas = this.casas;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterCasas = arr.filter( (e) => {
        return regexp.test(e.cas_rua_nome.split('')[0]);
      });
    }
  }

  setCasa(c){
    this.casa = 'Rua: ' + c.cas_rua_nome + ' | Quadra :' + c.cas_quadra + ' | Lote: '+ c.cas_lote;
    this.casaIndex = c.cas_id;
    this.state = false;
  }

  marcaState: boolean = false;
  resultFilterMarcas : any = [];

  filterMarcas(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterMarcas = this.marcas;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterMarcas = arr.filter( (e) => {
        return regexp.test(e.vm_nome.split('')[0]);
      });
    }
  }

  setMarca(m){
    this.marca = m.vm_nome;
    this.marcaIndex = m.vm_id;
    this.marcaState = false;
  }

  cores: any = [];

  cor: string = 'Selecione';
  corIndex: number = 0;

  corState: boolean = false;
  resultFilterCores : any = [];

  filterCores(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterCores = this.cores;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterCores = arr.filter( (e) => {
        return regexp.test(e.cor_nome.split('')[0]);
      });
    }
  }

  setCor(c){
    this.cor = c.cor_nome;
    this.corIndex = c.cor_id;
    this.corState = false;
  }


  modelos: any = [];

  modelo: string = 'Selecione';
  modeloIndex: number = 0;

  modeloState: boolean = false;
  resultFilterModelos : any = [];

  filterModelos(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterModelos = this.modelos;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterModelos = arr.filter( (e) => {
        return regexp.test(e.vmo_nome.split('')[0]);
      });
    }
  }

  setModelo(m){
    this.modelo = m.vmo_nome;
    this.modeloIndex = m.vmo_id;
    this.modeloState = false;
  }

  addModeloState: boolean = false;
  beforeScroll: number = 0;

  openAddModelo(){
    this.addModeloState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddModelo(){
    this.addModeloState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addModelo(){
    let valModelo = (<HTMLInputElement>document.getElementById('addModeloInput')).value;

    if (valModelo.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertModelo.php',
        {
          nome: valModelo,
          marca: this.marcaIndex
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.casas = [];
          this.cancelAddModelo();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }

  addMarcaState:boolean = false;

  openAddMarca(){
    this.addMarcaState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddMarca(){
    this.addMarcaState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addMarca(){
    let valMarca = (<HTMLInputElement>document.getElementById('addMarcaInput')).value;

    if (valMarca.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertMarca.php',
        {
          nome: valMarca
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.casas = [];
          this.cancelAddMarca();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }

}
