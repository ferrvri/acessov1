import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadMoradorComponent } from './cad-morador.component';

describe('CadMoradorComponent', () => {
  let component: CadMoradorComponent;
  let fixture: ComponentFixture<CadMoradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadMoradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadMoradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
