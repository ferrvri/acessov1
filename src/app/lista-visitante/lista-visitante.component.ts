import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-lista-visitante',
  templateUrl: './lista-visitante.component.html',
  styleUrls: ['./lista-visitante.component.css']
})
export class ListaVisitanteComponent implements OnInit {

  visitantes: any = [];

  imageDialog: boolean = false;

  pageParams: any;

  constructor(private _activatedRoute: ActivatedRoute, private _router: Router, private _http:Http) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'search'){
      this._http.post(
        'http://localhost/acessov1/selectVisitantes.php',
        {
          id: this.pageParams.params.query,
          unique: true
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            element.menuRow = false;
            this.visitantes.push(element);
          });
        }
      });
    }else{
      this._http.post(
        'http://localhost/acessov1/selectVisitantes.php',
        {}
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          response.json().result.forEach(element => {
            element.menuRow = false;
            this.visitantes.push(element);
          });
        }
      });
    }
  }

  beforeScroll : number = 0;
  image: string = '';

  openImageDialog(v){
    this.imageDialog = true;
    this.image = v.vis_foto;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  closeImageDialog(){
    this.imageDialog = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

}
