import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-historico-visita',
  templateUrl: './historico-visita.component.html',
  styleUrls: ['./historico-visita.component.css']
})
export class HistoricoVisitaComponent implements OnInit {

  pageParams: any;

  tableContent: any = [];
  imageDialog: boolean = false;

  constructor(private _activatedRoute: ActivatedRoute, private _router: Router, private _http: Http) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
  
    this._http.post(
      'http://localhost/acessov1/selectVisitantes.php',
      {
        id: this.pageParams.params.id
      }
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        let o = {
          nome: response.json().result[0].vis_nome,
          content: []
        };
        response.json().result.forEach(element => {
          o.content.push(element);
        });

        this.tableContent = o;
      }
    });
  }


  beforeScroll : number = 0;
  image: string = '';

  openImageDialog(m){
    console.log(m);
    this.imageDialog = true;
    this.image = m.vis_foto;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  closeImageDialog(){
    this.imageDialog = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

}
