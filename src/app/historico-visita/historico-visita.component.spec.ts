import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoVisitaComponent } from './historico-visita.component';

describe('HistoricoVisitaComponent', () => {
  let component: HistoricoVisitaComponent;
  let fixture: ComponentFixture<HistoricoVisitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoVisitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoVisitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
