import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-lista-carro',
  templateUrl: './lista-carro.component.html',
  styleUrls: ['./lista-carro.component.css']
})
export class ListaCarroComponent implements OnInit {

  pageParams: any;
  tableContent: any = [];

  constructor( private _activatedRoute: ActivatedRoute, private _http: Http, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    this._http.post(
      'http://localhost/acessov1/selectCarroMorador.php',
      {
        id: this.pageParams.params.id
      }
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.tableContent.push(element);  
        });
      }else if (response.json().status == '0x101'){
        window.alert('\tMNTI - Solutions\r\n\r\nErro ao selecionar carros do proprietario!');
      }
    });
  }

}
