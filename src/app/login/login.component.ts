import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';

declare var Date: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error = {message:''};

  selectLocal: boolean = false;

  portariaState: boolean = false;
  portaria: string = 'Selecionar portaria';
  portariaIndex: number = 0;

  errorPortaria = {required: null};

  u: string = '';
  p: string = '';

  pageParams: any;

  constructor(private _http: Http, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if (localStorage.getItem('session') ){
      this._router.navigate(['main']);
    }
    if (!localStorage.getItem('local')){
      this.selectLocal = true;
    }else{
      this.selectLocal = false;
    }
    document.getElementById('userInput').focus();
  }

  setPortaria(index){
    switch(index){
      case 0:
        this.portariaIndex = 1;
        this.portaria = 'Portaria 1';
      break;

      case 1:
        this.portariaIndex = 2;
        this.portaria = 'Portaria 2';
      break;
      
      case 2:
        this.portariaIndex = 3;
        this.portaria = 'Portaria 3';
      break;
    }
    this.portariaState = false;
  }

  login(u, p){
    if ( ((u.length < 1 || u === undefined) || (p.length < 1 || p === undefined) )){
      this.error.message = 'Preencha os campos';    
      setTimeout( () => {
        this.error.message = '';
      }, 1500);
    }else if (this.portaria.length < 1 || this.portaria == 'Selecionar portaria'){
      this.errorPortaria.required = true;
      setTimeout( () => {
        this.errorPortaria.required = null;
      }, 1500);
    }else{
      this._http.post(
        'http://localhost/acessov1/login.php',
        {
          user: u,
          pass: p
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          localStorage.setItem('local', ''+this.portariaIndex);
          let obj = {usu_id: response.json().result[0].usu_id, usu_nome: response.json().result[0].usu_nome, usu_email: response.json().result[0].usu_email, expires: Date.today()};
          localStorage.setItem('session', JSON.stringify(obj));
          this._router.navigate(['loadingfull', {page: 'main'}]);
        }else{
          window.alert('\t    MNTI Solutions\r\n\r\nUsuario ou senha incorretos!');
        }
      });
    }
  }

  focusPass(e: KeyboardEvent){
    switch(e.keyCode){
      case 13:
        document.getElementById('passInput').focus();
      break;
    }
  }

  loginKey(e: KeyboardEvent){
    switch(e.keyCode){
      case 13:
        this.login(this.u, this.p);
      break;
    }
  }

}
