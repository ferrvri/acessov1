import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaVisitaComponent } from './nova-visita.component';

describe('NovaVisitaComponent', () => {
  let component: NovaVisitaComponent;
  let fixture: ComponentFixture<NovaVisitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaVisitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaVisitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
