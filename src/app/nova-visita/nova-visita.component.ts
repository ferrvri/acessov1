import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-nova-visita',
  templateUrl: './nova-visita.component.html',
  styleUrls: ['./nova-visita.component.css']
})
export class NovaVisitaComponent implements OnInit {

  pageParams: any;

  errorModelo = {required: null};
  errorMarca = {required: null};
  errorCor = {required: null};
  errorPlaca = {required: null};
  errorMorador = {required: null};
  errorCartao = {required: null};

  moradores:any = [];
  cores: any = [];
  modelos: any = [];
  marcas:any = [];

  modelo: string = 'Selecione';
  marca: string = 'Selecione';
  placa: string = '';
  cor: string = 'Selecione';

  cartao: string = '';

  constructor(private _activatedRoute: ActivatedRoute, private _http: Http, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });  

    this._http.post(
      'http://localhost/acessov1/selectMoradores.php',
      {
        search: false
      }
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.moradores.push(element);
        });
        this.resultFilterMorador = this.moradores;
      }else if (response.json().status == '0x101'){
        window.alert('Erro ao selecionar moradores');
      }
    });

    this._http.post(
      'http://localhost/acessov1/selectMarcas.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.marcas.push(element);
        });
        this.resultFilterMarcas = this.marcas;
      }
    });

    this._http.post(
      'http://localhost/acessov1/selectCores.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.cores.push(element);
        });
        this.resultFilterCores = this.cores;
      }
    });

    this._http.post(
      'http://localhost/acessov1/selectModelos.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        console.log(response.json());
        response.json().result.forEach(element => {
          this.modelos.push(element);
        });
        this.resultFilterModelos = this.modelos;
      }
    });
  }

  validation(){
    try{
      if (this.morador.length < 1 || this.morador == 'Selecionar'){
        this.errorMorador.required = true;
      }else{
        this.errorMorador.required = null;
      }

      if (this.placa.length < 1){
        this.errorPlaca.required = true
      }else{
        this.errorPlaca.required = null;
      }

      if (this.modelo.length < 1 || this.modelo == 'Selecione'){
        this.errorModelo.required = true
      }else{
        this.errorModelo.required = null;
      }

      if (this.marca.length < 1 || this.marca == 'Selecione'){
        this.errorMarca.required = true
      }else{
        this.errorMarca.required = null;
      }

      if (this.cor.length < 1 || this.cor == 'Selecione'){
        this.errorCor.required = true
      }else{
        this.errorCor.required = null;
      }

      if (this.cartao.length < 1){
        this.errorCartao.required = true
      }else{
        this.errorCartao.required = null;
      }

      return true;
    }catch(e){
      return false;
    }
  }

  resultFilterMorador:any = [];
  moradorIndex: number = 0;
  moradorState: boolean = false;
  morador: string = 'Selecione';

  filterMoradores(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterMorador = this.moradores;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterMorador = arr.filter( (e) => {
        return regexp.test(e.mor_nome.split('')[0]);
      });
    }
  }

  setMorador(m){
    this.morador = 'Morador: '+ m.mor_nome + ' | Telefone: '+ m.mor_telefone +' | Rua: ' + m.cas_rua_nome + ' | Quadra: ' + m.cas_quadra + ' | Lote: '+ m.cas_lote;
    this.moradorIndex = m.mor_id;
    this.moradorState = false;
  }


  cadVisita(){
    if (this.morador.length < 1 || this.morador == 'Selecionar'){
      window.alert('\r\tMNTI - Solution\r\n\r\nSelecione uma opção para continuar');
    }else if (this.validation() == true && this.cor.length > 0 && this.cartao.length > 0 && this.marca.length > 0 &&
    this.modelo.length > 0 && this.placa.length > 0){
      this._http.post(
        'http://localhost/acessov1/insertVisita.php',
        {
          modelo: this.modeloIndex,
          marca: this.marcaIndex,
          cor: this.corIndex,
          placa: this.placa,
          fk_morador: this.moradorIndex,
          fk_visita: this.pageParams.params.id 
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this._http.post(
            'http://localhost/acessov1/insertRegisto.php',
            {
              visitante : true,
              visitante_id: this.pageParams.params.id,
              cartao: this.cartao,
              morador: this.moradorIndex,
              local: localStorage.getItem('local')
            }
          ).subscribe( (response) => {
            if (response.json().status == '0x104'){
              window.alert('\r\tMNTI - Solutions\r\n\r\Nova visita registrada com sucesso!');
              this._router.navigate(['/main/innerload', {page: '/main/listaVisitante'}]);
            }else if (response.json().status == '0x101'){
              window.alert('Erro ao incluir registro de entrada');
            }
          });
        }else if (response.json().status == '0x101'){
          window.alert('Erro ao cadastrar nova visita');
        }
      });
    }
  }

  marcaIndex: number = 0;
  marcaState: boolean = false;
  resultFilterMarcas : any = [];

  filterMarcas(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterMarcas = this.marcas;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterMarcas = arr.filter( (e) => {
        return regexp.test(e.vm_nome.split('')[0]);
      });
    }
  }

  setMarca(m){
    this.marca = m.vm_nome;
    this.marcaIndex = m.vm_id;
    this.marcaState = false;
  }

  corIndex: number = 0;
  corState: boolean = false;
  resultFilterCores : any = [];

  filterCores(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterCores = this.cores;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterCores = arr.filter( (e) => {
        return regexp.test(e.cor_nome.split('')[0]);
      });
    }
  }

  setCor(c){
    this.cor = c.cor_nome;
    this.corIndex = c.cor_id;
    this.corState = false;
  }

  modeloIndex: number = 0;

  modeloState: boolean = false;
  resultFilterModelos : any = [];

  filterModelos(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterModelos = this.modelos;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterModelos = arr.filter( (e) => {
        return regexp.test(e.vmo_nome.split('')[0]);
      });
    }
  }

  setModelo(m){
    this.modelo = m.vmo_nome;
    this.modeloIndex = m.vmo_id;
    this.modeloState = false;
  }

  addModeloState: boolean = false;
  beforeScroll: number = 0;

  openAddModelo(){
    this.addModeloState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddModelo(){
    this.addModeloState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addModelo(){
    let valModelo = (<HTMLInputElement>document.getElementById('addModeloInputNovaVisita')).value;

    if (valModelo.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertModelo.php',
        {
          nome: valModelo,
          marca: this.marcaIndex
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.moradores = [];
          this.cancelAddModelo();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }

  addMarcaState:boolean = false;

  openAddMarca(){
    this.addMarcaState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddMarca(){
    this.addMarcaState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addMarca(){
    let valMarca = (<HTMLInputElement>document.getElementById('addMarcaInputNovaVisita')).value;

    if (valMarca.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertMarca.php',
        {
          nome: valMarca
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.moradores = [];
          this.cancelAddMarca();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }


}
