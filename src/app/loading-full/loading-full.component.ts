import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-loading-full',
  templateUrl: './loading-full.component.html',
  styleUrls: ['./loading-full.component.css']
})
export class LoadingFullComponent implements OnInit {

  pageParams:any;

  constructor( private _activatedRoute: ActivatedRoute, private _router: Router ) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    setTimeout( () => {
      this._router.navigate([this.pageParams.params.page]);
    }, 1500);
  }

}
