import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-innerload',
  templateUrl: './innerload.component.html',
  styleUrls: ['./innerload.component.css']
})
export class InnerloadComponent implements OnInit {

  pageParams: any = [];

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
    setTimeout( () => {
      this._router.navigate([this.pageParams.params.page]);
    }, 1500);
  }



}
