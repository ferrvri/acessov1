import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerloadComponent } from './innerload.component';

describe('InnerloadComponent', () => {
  let component: InnerloadComponent;
  let fixture: ComponentFixture<InnerloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
