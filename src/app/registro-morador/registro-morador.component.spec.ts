import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroMoradorComponent } from './registro-morador.component';

describe('RegistroMoradorComponent', () => {
  let component: RegistroMoradorComponent;
  let fixture: ComponentFixture<RegistroMoradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroMoradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroMoradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
