import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-registro-morador',
  templateUrl: './registro-morador.component.html',
  styleUrls: ['./registro-morador.component.css']
})
export class RegistroMoradorComponent implements OnInit {

  tableContent: any = [];
  pageParams: any;

  constructor(private _http: Http, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    this._http.post(
      'http://localhost/acessov1/selectMoradorRegistro.php',
      {
        id: this.pageParams.params.id
      }
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.tableContent.push(element);
        });
      }
    });
  }

}
