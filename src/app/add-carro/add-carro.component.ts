import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-carro',
  templateUrl: './add-carro.component.html',
  styleUrls: ['./add-carro.component.css']
})
export class AddCarroComponent implements OnInit {
  
  marca: string = 'Selecione';
  marcas: any = [];
  marcaIndex: number = 0;

  placa: string = '';

  errorModelo = {required: null};
  errorMarca = {required: null};
  errorCor = {required: null};
  errorPlaca = {required: null};

  pageParams: any;

  constructor( private _http: Http, private _router: Router, private _activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
    this._http.post(
      'http://localhost/acessov1/selectMarcas.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.marcas.push(element);
        });
        this.resultFilterMarcas = this.marcas;
      }
    });

    this._http.post(
      'http://localhost/acessov1/selectCores.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        response.json().result.forEach(element => {
          this.cores.push(element);
        });
        this.resultFilterCores = this.cores;
      }
    });

    this._http.post(
      'http://localhost/acessov1/selectModelos.php',
      {}
    ).subscribe( (response) => {
      if (response.json().status == '0x104'){
        console.log(response.json());
        response.json().result.forEach(element => {
          this.modelos.push(element);
        });
        this.resultFilterModelos = this.modelos;
      }
    });
  }

  marcaState: boolean = false;
  resultFilterMarcas : any = [];

  filterMarcas(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterMarcas = this.marcas;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterMarcas = arr.filter( (e) => {
        return regexp.test(e.vm_nome.split('')[0]);
      });
    }
  }

  setMarca(m){
    this.marca = m.vm_nome;
    this.marcaIndex = m.vm_id;
    this.marcaState = false;
  }

  cores: any = [];

  cor: string = 'Selecione';
  corIndex: number = 0;

  corState: boolean = false;
  resultFilterCores : any = [];

  filterCores(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterCores = this.cores;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterCores = arr.filter( (e) => {
        return regexp.test(e.cor_nome.split('')[0]);
      });
    }
  }

  setCor(c){
    this.cor = c.cor_nome;
    this.corIndex = c.cor_id;
    this.corState = false;
  }


  modelos: any = [];

  modelo: string = 'Selecione';
  modeloIndex: number = 0;

  modeloState: boolean = false;
  resultFilterModelos : any = [];

  filterModelos(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterModelos = this.modelos;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterModelos = arr.filter( (e) => {
        return regexp.test(e.vmo_nome.split('')[0]);
      });
    }
  }

  setModelo(m){
    this.modelo = m.vmo_nome;
    this.modeloIndex = m.vmo_id;
    this.modeloState = false;
  }

  addModeloState: boolean = false;
  beforeScroll: number = 0;

  openAddModelo(){
    this.addModeloState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddModelo(){
    this.addModeloState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addModelo(){
    let valModelo = (<HTMLInputElement>document.getElementById('addModeloInputAddCarro')).value;

    if (valModelo.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertModelo.php',
        {
          nome: valModelo,
          marca: this.marcaIndex
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.cancelAddModelo();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }

  addMarcaState:boolean = false;

  openAddMarca(){
    this.addMarcaState = true;
    document.body.scrollTop = 0;
    document.getElementById('body').scrollTop = 0;
    this.beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflow = 'hidden';
  }

  cancelAddMarca(){
    this.addMarcaState = false;
    document.documentElement.scrollTop = this.beforeScroll;
    document.getElementById('body').style.overflow = 'auto';
  }

  addMarca(){
    let valMarca = (<HTMLInputElement>document.getElementById('addMarcaInputAddCarro')).value;

    if (valMarca.length < 1){
      window.alert('Preencha o campo para adicionar!');
    }else{
      this._http.post(
        'http://localhost/acessov1/insertMarca.php',
        {
          nome: valMarca
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          this.marcaState = false;
          this.modelos = [];
          this.cores = [];
          this.marcas = [];
          this.cancelAddMarca();
          this.ngOnInit();
        }else{
          window.alert('\r\tMNTI - Solutions\r\n\r\nErro na inserção de novo modelo');
        }
      });
    }
  }

  validation(){
    try{

      if(this.marca.length < 1 || this.marca == 'Selecione'){
        this.errorMarca.required = true;
      }else{
        this.errorMarca.required = null;
      }

      if(this.modelo.length < 1 || this.modelo == 'Selecione'){
        this.errorModelo.required = true;
      }else{
        this.errorModelo.required = null;
      }

      if(this.cor.length < 1 || this.cor == 'Selecione'){
        this.errorCor.required = true;
      }else{
        this.errorCor.required = null;
      }

      if(this.placa.length < 1){
        this.errorPlaca.required = true;
      }else{
        this.errorPlaca.required = null;
      }
      return true;
    }catch(e){
      return false;
    }
  }

  addCarro(){
    if (this.validation() == true && this.placa.length > 0 && this.modelo.length > 0
    && this.marca.length > 0 && this.cor.length > 0){
      this._http.post(
        'http://localhost/acessov1/insertMoradorVeiculo.php',
        {
          id: this.pageParams.params.id,
          placa: this.placa,
          cor: this.corIndex,
          modelo: this.modeloIndex,
          marca: this.marcaIndex
        }
      ).subscribe( (response) => {
        if (response.json().status == '0x104'){
          window.alert('\tMNTI - Solutions\r\n\r\nVeiculo vinculado ao morador!');
          this._router.navigate(['/main/innerload', {page: '/main/listaMorador'}]);
        }else if (response.json().status == '0x101'){
          window.alert('Erro na inserção de novo veiculo a esse morador');
        }
      });
    }
  }


}
